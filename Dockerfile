FROM ubuntu:16.04

MAINTAINER Matteo Bachetti <matteo@matteobachetti.it>

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN apt-get update -y && apt-get update -y && \
    apt-get install -y build-essential make gcc cmake git swig doxygen \
    python-sphinx bzip2 g++ expat freeglut3-dev gfortran wget \
    libexpat1-dev libx11-dev libxmu-dev libxt-dev

RUN useradd -m utentepecora && echo "utentepecora:utentepecora" | chpasswd && adduser utentepecora sudo

ENV HOME /home/utentepecora

WORKDIR $HOME

COPY gpdext gpdext

RUN chown -R utentepecora:utentepecora /home/utentepecora

USER utentepecora

RUN wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh

RUN bash miniconda.sh -b -p $HOME/miniconda

ENV PATH "$HOME/miniconda/bin:$PATH"

WORKDIR $HOME/gpdext

RUN pwd

RUN /bin/bash -c "source setup.sh && make cfitsio"

RUN /bin/bash -c "source setup.sh && make boost"

RUN /bin/bash -c "source setup.sh && make qt"

RUN /bin/bash -c "source setup.sh && make geant"

CMD [ "/bin/bash" ]
