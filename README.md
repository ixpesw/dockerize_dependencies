How to use the Docker dependency image
--------------------------------------

The image is based on Ubuntu 16.04, but it is easy to modify to be used with other
distributions as well.

## Instructions

1. Install [Docker](https://www.docker.com/)

2. Clone the repository:


```
    $ git clone user@bitbucket.org:ixpesw/dockerize_dependencies.git
    $ cd dockerize_dependencies
```

3. Copy the gpdext directory into dockerize_dependencies:

```
    $ cp -r /path/to/gpdext .
```

4. Clean everything:

```
    $ cd gpdext && git clean -dxf && cd -
```

5. Build the image!

```
    $ docker build .
```

If the process doesn't fail, everything works. As easy as pie
